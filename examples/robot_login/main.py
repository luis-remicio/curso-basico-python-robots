# -*- encoding: utf-8 -*-

import os, sys
import wx
import wx.aui

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

from form import DemoGUI

class ProcessAdminRobotApp(wx.App):

    def OnInit(self):
        frame = DemoGUI(None)
        frame.Show(True)
        return True

if __name__ == '__main__':
    app = ProcessAdminRobotApp(0)
    app.MainLoop()