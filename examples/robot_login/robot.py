# -*- encoding: utf-8 -*-

# ###################################################################
# Desarrollado por Luis Remicio
# ###################################################################
# Ingreso automatico PRAXIS
# ###################################################################

import sys, os
import pyodbc
import pyautogui
import clipboard
import keyboard
import cv2
import numpy as np
import time
from PIL import Image
from datetime import datetime, date, timedelta
from collections import namedtuple

class RobotLogin:

    database_server = 'POWER7'
    database_user = 'XXXXXX'
    database_password = 'XXXXXXX'

    conx = None
    conn = None

    base_path = ''
    path_robot = ''

    timeOut = 3600
    timeStart = 0
    objExiste = {}

    def __init__(self):

        try:
            self.base_path = sys._MEIPASS
            self.path_robot = self.base_path
        except:
            self.base_path = os.path.abspath(os.path.dirname(__file__))
            self.path_robot = self.base_path
        
        self.getConnection()

    '''
    Establecemos la conexión vía ODBC
    '''
    def getConnection(self):
        self.conx = pyodbc.connect('DSN=' + self.database_server + ';UID=' + self.database_user + ';PWD=' + self.database_password, autocommit=True)
        self.conx.setencoding('utf-8')
        self.conn = self.conx.cursor()
        return self

    def findImage(self, *args, **kwargs):
        response = {}
        timeStart = 0
        response['status'] = False
        timeO = kwargs.get('timeout') if kwargs.get('timeout') != None else self.timeOut
        grayscaleO = kwargs.get('grayscale') if kwargs.get('grayscale') != None else True
        vexcept = kwargs.get('vexcept') if kwargs.get('vexcept') != None else []
        while timeStart != timeO:
            time.sleep(0.01)
            timeStart = timeStart + 1
            cor = 0
            location = None
            for image in os.scandir(kwargs.get('path')):
                cor = image.name.split('_')[1].split('.')[0]
                print('=>', str(kwargs.get('path') + image.name), '<->', str(grayscaleO))
                if kwargs.get('region') != None:
                    location = pyautogui.locateOnScreen(kwargs.get('path') + image.name, grayscale=grayscaleO, region = kwargs.get('region'))
                else:
                    location = pyautogui.locateOnScreen(kwargs.get('path') + image.name, grayscale=grayscaleO)
                if int(cor) not in vexcept:
                    if location != None:
                        break
            if location != None:
                response['status'] = True
                response['location'] = location
                response['flag'] = int(cor)
                break
            else:
                if kwargs.get('function') != None:
                    eval(str(kwargs.get('function')))
                else:
                    pyautogui.moveRel(-1,0)

        return response

    '''
    Retorna coordenadas de una imagen con distintos grados de precision
    '''
    def imagesearch(self, image, precision=0.8):
        im = pyautogui.screenshot()
        img_rgb = np.array(im)
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
        template = cv2.imread(image, 0)
        template.shape[::-1]

        res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        if max_val < precision:
            return [-1,-1]
        return max_loc

    def findImagePre(self, *args, **kwargs):
        response = {}
        response['items'] = []
        timeStart = 0
        timeO = kwargs.get('timeout') if kwargs.get('timeout') != None else self.timeOut
        grayscaleO = kwargs.get('grayscale') if kwargs.get('grayscale') != None else True
        unique = kwargs.get('unique') if kwargs.get('unique') != None else False
        ValoresLocation = namedtuple('ValoresLocation',['x', 'y', 'index', 'status'])
        img_exists = False
        if not unique:
            for image in os.scandir(kwargs.get('path')):
                img = cv2.imread(kwargs.get('path') + image.name)
                height, width, channels = img.shape

                cor = image.name.split('_')[1].split('.')[0]
                location = self.imagesearch(kwargs.get('path') + image.name)

                img_exists = True if(location[0] >= 0 and location[1] >= 0) else False

                if location != None:
                    response['items'].append({
                        'location': ValoresLocation(
                                (location[0] + (width / 2)) if img_exists else -1, 
                                (location[1] + (height / 2)) if img_exists else -1,
                                int(cor) if img_exists else -1,
                                img_exists)
                    })
        else:
            response = {}
            while timeStart != timeO:
                time.sleep(0.01)
                timeStart = timeStart + 1
                cor = 0
                location = None
                for image in os.scandir(kwargs.get('path')):
                    img = cv2.imread(kwargs.get('path') + image.name)
                    height, width, channels = img.shape

                    cor = image.name.split('_')[1].split('.')[0]
                    location = self.imagesearch(kwargs.get('path') + image.name)
                    print('=>', str(kwargs.get('path') + image.name), '<->', str(grayscaleO))
                    img_exists = True if(location[0] >= 0 and location[1] >= 0) else False
                    if img_exists:
                        break

                if img_exists:
                    response['status'] = True
                    response['location'] = ValoresLocation(
                                (location[0] + (width / 2)) if img_exists else -1, 
                                (location[1] + (height / 2)) if img_exists else -1,
                                int(cor) if img_exists else -1,
                                img_exists
                    )
                    break
                else:
                    response['status'] = False
                    pyautogui.moveRel(-1,0)

        return response

    def startApp(self, *args, **kwargs):
        os.system('start chrome "http://10.19.17.40/PRAXIS/"')

        try:
            pyautogui.FAILSAFE = True
            time.sleep(2)
            confirma = pyautogui.confirm(text='Iniciar proceso automatizado?', title='Robot', buttons=['OK', 'Cancel'])
            if confirma == 'OK':
                rs = self.getData(
                    IN_USER = 'GUESTPRX'
                )
                for row in rs:
                    print(row)
                    self.paso_0001(row)

                pyautogui.alert(text='El proceso culminó exitósamente', title='Robot BSP', button='OK')
            else:
                print('Proceso cancelado!')
        except pyautogui.FailSafeException as err:
            print(err)
            pyautogui.alert(text='Se interrumpió el proceso!', title='Robot', button='OK')

    def getData(self, *args, **kwargs):
        query = """\
            select USR, CITY, USCR from PRAXIS.INF001
            where USR = ?
        """
        params = (
            kwargs.get('IN_USER'),
        )
        rs = self.conn.execute(query,params).fetchall()
        return rs

    def paso_0001(self, row):
        print('paso_0001')
        self.objExiste = self.findImage(path=self.path_robot + '/images/img_001/')
        if self.objExiste['status']:
            self.paso_0002(row)

    def paso_0002(self, row):
        print('paso_0002')
        self.objExiste = self.findImage(path=self.path_robot + '/images/img_002/')
        if self.objExiste['status']:
            locationx, locationy = pyautogui.center(self.objExiste['location'])
            pyautogui.moveTo(locationx + 50, locationy)
            time.sleep(1)
            pyautogui.click(clicks=2, interval=0.5)
            
            time.sleep(1)
            keyboard.send('ctrl+a,del')
            time.sleep(1)
            pyautogui.typewrite('GUESTPRX')
            pyautogui.press('tab')
            time.sleep(1)

            keyboard.send('ctrl+a,del')
            time.sleep(1)
            pyautogui.typewrite('AS2015')
            pyautogui.press('tab')
            time.sleep(1)

            keyboard.send('enter')

            self.paso_0003(row)

    def paso_0003(self, row):
        print('paso_0003')
        self.objExiste = self.findImage(path=self.path_robot + '/images/img_003/')
        if self.objExiste['status']:
            locationx, locationy = pyautogui.center(self.objExiste['location'])
            pyautogui.moveTo(locationx, locationy)
            time.sleep(1)
            pyautogui.click()

            self.paso_0004(row)

    def paso_0004(self, row):
        print('paso_0004')
        self.objExiste = self.findImage(path=self.path_robot + '/images/img_004/')
        if self.objExiste['status']:
            pass
            # self.log_screen()

    def log_screen(self):
        im = pyautogui.screenshot()
        im.save(self.path_robot + '\\screenshot\\' + time.strftime("%Y%m%d") + '_img.png')
        list_of_pixels = list(im.getdata())
        im2 = Image.new(im.mode, im.size)
        im2.putdata(list_of_pixels)

# if __name__ == '__main__':
#     app = RobotLogin()
#     app.startApp()