
# Tutorial básico de python #
### ( Orientado al desarrollo de procesos automatizados ) ###

1. ¿ Qué es python ?
2. Instalación y configuración
3. Entorno virtual y su importancia
4. Operadores matemáticos
5. Variables
6. Operadores Relacionales
7. Operadores Lógicos
8. Operadores de Incremento/Decremento
9. Estructuras condicionales
10. Estructuras de datos
    - Listas

    - Diccionarios

    - Tuplas
11. Funciones
12. Programación Orientada a objetos
13. Manejo de errores
14. Robots

    - Ejemplo de ingreso automático PRAXIS ( Estandar / Personalizada )
15. Interfaces gráficas
16. Generación de ejecutable
17. Source code
18. Links de interés 







------

1. ### ¿Qué es python? ###

   - Python es un lenguaje de programación creado por Guido Van Rossum, con una sintaxis muy limpia, ideado para enseñar a la gente a programar bien. Se trata de un lenguaje interpretado o de script.

     **Ventajas**:

     - **Legible:** sintaxis intuitiva y estricta.
     - **Productivo:** ahorra mucho código.
     - **Portable:** para  todo sistema operativo.
     - **Recargado:** viene con muchas librerías por defecto.

2. ### Instalación y configuración ### 

   - Existen dos versiones de Python que tienen gran uso actualmente, *Python 2.x* y *Python 3.x*, para este curso necesitas usar una versión 3.x

     Para instalar Python solo debes seguir los pasos dependiendo del sistema operativo que tengas instalado.

     **Windows**

     Para instalarlo en Windows, debemos ir a <https://www.python.org/downloads/> el sitio reconocerá el sistema operativo y te dará las opciones para descargar.

     Ejecuta el archivo que descargaste y sigue los pasos de instalación. Al finalizar vas a poder utilizar Python en tu computador y estás listo para continuar con el curso.

     ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0001.png)

     Para verificar que todo se haya instalado correctamente sola ingresar a la terminal de línea de comandos e ingresar lo siguiente: ``python --version`` , y te debe de mostrar algo similar a esto:

     ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0002.png) 

     

   - ***PIP*** 

     pip es un sistema de gestión de paquetes utilizado para instalar y administrar paquetes de software escritos en Python.

     Para instalar un paquete en específico: ``pip install django``

     Para realizar instalaciones por lotes, crear en el directorio de instalación un archivo ***"requirements.txt"*** , en el fichero creado ingresar el nombre del paquete con la versión a instalar, luego para instalar todos los paquetes, desde la línea de comandos ingresar lo siguiente: ``pip install -r requirements.txt`` 

3. ### Entorno virtual y su importancia ### 

   - Es una herramienta que ayuda a mantener el entorno de desarrollo ordenado en tu computadora, es posible saltarse este paso, pero es altamente recomendable.

     Para crear un nuevo entorno virtual ingresar el siguiente comando en la terminal: 

     ``python3 -m venv myvenv``  

4. ### Operadores matemáticos ### 

   - +
   - -
   - *
   - /
   - //
   - %
   - *
   - **

5. ### Variables y expresiones 

   - Las variables, a diferencia de los demás lenguajes de programación, no debes definirlas, ni tampoco su tipo de dato, ya que al momento de iterarlas se identificará su tipo. Recuerda que en Python todo es un objeto.

     ```python
     a = 3
     b = a
     ```

6. ### Operadores Relacionales

   - Se utilizan para evaluar condicionales; como respuesta obtendremos booleanos.

     | Símbolo | Operación     | Ejemplo | Resultado |
     | ------- | ------------- | ------- | --------- |
     | ==      | Igual que     | 5==5    | True      |
     | !=      | Diferente que | 4!=5    | True      |
     | >       | Mayor que     | 98>100  | False     |
     | <       | Menor que     | 12<39   | True      |
     | >=      | Mayor o igual | 6>=6    | True      |
     | <=      | Menor o igual | 3<=8    | True      |

7. ### Operadores Lógicos

   - Permite construir expresiones lógicas, se obtiene como resultado ***booleanos***.

     | **Símbolo** | **Operación** | **Ejemplo**   | **Resultado** |
     | ----------- | ------------- | ------------- | ------------- |
     | **and**     | Conjunción    | 12>2 and 5<10 | True          |
     | **or**      | Disyunción    | 9!=6 or 8<=5  | True          |
     | **not**     | Negación      | not True      | False         |

8. ### Operadores de Incremento/Decremento

   | **Símbolo** | **Ejemplo** | **Equivalente a** |
   | ----------- | ----------- | ----------------- |
   | **+=**      | A+=5        | A=A+5             |
   | **-=**      | A-=5        | A=A-5             |
   | ***=**      | A*=5        | A=A*5             |
   | **/=**      | A/=5        | A=A/5             |
   | **%=**      | A%=5        | A=A%5             |

9. ### Estructuras condicionales

   - **Condicionales IF**

     Los condicionales tienen la siguiente estructura. Ten en cuenta que lo que contiene los paréntesis es la comparación que debe cumplir para que los elementos se cumplan.

     ```python
      if ( a > b ):
      	elementos 
      elif ( a == b ): 
      	elementos 
      else:
      	elementos
     ```

   - **Bucle FOR**

     El bucle de for lo puedes usar de la siguiente forma: recorres una cadena o lista a la cual va a tomar el elemento en cuestión con la siguiente estructura:

     ```python
     for i in ____:
      	elementos
     ```

     Ejemplo:

     ```python
      for i in range(10):
      	print(i)
     ```

   - **Bucle WHILE**

     En este caso while tiene una condición que determina hasta cuándo se ejecutará. O sea que dejará de ejecutarse en el momento en que la condición deje de ser cierta. La estructura de un while es la siguiente:

     ```python
      while (condición):
      	elementos
     ```

     Ejemplo:

     ```python
      >>> x = 0 
      >>> while x < 10: 
      ... 	print x 
      ... 	x += 1
     ```

     

10. ### Estructuras de datos

    - **Listas** 

      Las listas las declaras con corchetes. Estas pueden tener una lista dentro o cualquier tipo de dato.

      ```python
       >>> L = [22, True, ”una lista”, [1, 2]] 
       >>> L[0] 
       22
      ```

    - **Diccionarios**

      En los diccionarios tienes un grupo de datos con un formato: la primera cadena o número será la clave para acceder al segundo dato, el segundo dato será el dato al cual accederás con la llave. Recuerda que los diccionarios son listas de llave:valor.

      ```python
       >>> D = {"Kill Bill": "Tamarino", "Amelie": "Jean-Pierre Jeunet"} 
       >>> D["Kill Bill"]
       "Tamarino"
      ```

    - **Tuplas**

      Las tuplas se declaran con paréntesis, recuerda que no puedes editar los datos de una tupla después de que la has creado.

      ```python
       >>> T = (22, True, "una tupla", (1, 2)) 
       >>> T[0] 
       22
      ```

11. ### Funciones

    - Las funciones las defines con **def** junto a un nombre y unos paréntesis que reciben los parámetros a usar. Terminas con dos puntos.

      **def** nombre_de_la_función(**parametros**):

      Después por indentación colocas los datos que se ejecutarán desde la función:

      ```python
       >>> def my_first_function():
       ...	return “Hello World!” 
       ...    
       >>> my_first_function()
      ```

12. ### Programación Orientada a objetos

    - **Clases**

      Clases es uno de los conceptos con más definiciones en la programación, pero en resumen sólo son la representación de un objeto. Para definir la clase usas_ class_ y el nombre. En caso de tener parámetros los pones entre paréntesis.

      Para crear un constructor haces una función dentro de la clase con el nombre **init** y de parámetros self (significa su clase misma), nombre_r y edad_r:

      ```python
       >>> class Estudiante(object): 
       ... 	def __init__(self,nombre_r,edad_r): 
       ... 		self.nombre = nombre_r 
       ... 		self.edad = edad_r 
       ...
       ... 	def hola(self): 
       ... 		return "Mi nombre es %s y tengo %i" % (self.nombre, self.edad) 
       ... 
       >>> e = Estudiante(“Arturo”, 21) 
       >>> print(e.hola()) 
       Mi nombre es Arturo y tengo 21
      ```

      

13. ### Manejo de errores

    - Un programa de Python termina cuando se encuentra un error.

      - Es diferente a un error de sintaxis

        ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0003.png)

        

14. ### Robots

    - **[PyAutoGUI](https://pyautogui.readthedocs.io)**

      El propósito de PyAutoGUI es proporcionar un módulo Python multiplataforma para la automatización de GUI. El API está diseñada para ser lo más simple posible con valores predeterminados razonables.

      #### Requisitos 

      - Antes de empezar recuerda que tienes que tener instalado los paquetes necesarios para el desarrollo de robots, adjunto estaré dejando un archivo **"requirements.txt"** con las dependencias necesarias, abre la terminal, ubícate en el directorio en el que se encuentre el archivo e ingresar lo siguiente: ``pip install -r requirements.txt ``, este comando se encargará de instalar todo lo necesario para empezar a desarrollar robots (sino deseas instalar tantos paquetes, puedes instalar de forma manual solamente los paquetes necesarios, pero recuerda que el archivo de dependencias que te estoy adjuntando esta optimizado para este tipo de procesos).

      - **Configurar acceso a datos ODBC**

        ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0004.png)

        ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0005.png)

        ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0006.png)

        ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0007.png)

      #### *Ejemplo:*

      ##### **Robot de ingreso automático a PRAXIS**

      ```python
      # -*- encoding: utf-8 -*-
      
      # ###################################################################
      # Desarrollado por Luis Remicio
      # ###################################################################
      # Ingreso automatico PRAXIS
      # ###################################################################
      
      import sys, os
      import pyodbc
      import pyautogui
      import clipboard
      import keyboard
      import cv2
      import numpy as np
      import time
      from PIL import Image
      from datetime import datetime, date, timedelta
      from collections import namedtuple
      
      class RobotLogin:
      
          database_server = 'POWER7'
          database_user = 'GUESTPRX'
          database_password = 'AS2015'
      
          conx = None
          conn = None
      
          base_path = ''
          path_robot = ''
      
          timeOut = 3600
          timeStart = 0
          objExiste = {}
      
          def __init__(self):
      
              try:
                  self.base_path = sys._MEIPASS
              except:
                  self.base_path = os.path.abspath(os.path.dirname(__file__))
                  self.path_robot = self.base_path
              
              self.getConnection()
      
          '''
          Establecemos la conexión vía ODBC
          '''
          def getConnection(self):
              self.conx = pyodbc.connect('DSN=' + self.database_server + ';UID=' + self.database_user + ';PWD=' + self.database_password, autocommit=True)
              self.conx.setencoding('utf-8')
              self.conn = self.conx.cursor()
              return self
      
          def findImage(self, *args, **kwargs):
              response = {}
              timeStart = 0
              response['status'] = False
              timeO = kwargs.get('timeout') if kwargs.get('timeout') != None else self.timeOut
              grayscaleO = kwargs.get('grayscale') if kwargs.get('grayscale') != None else True
              vexcept = kwargs.get('vexcept') if kwargs.get('vexcept') != None else []
              while timeStart != timeO:
                  time.sleep(0.01)
                  timeStart = timeStart + 1
                  cor = 0
                  location = None
                  for image in os.scandir(kwargs.get('path')):
                      cor = image.name.split('_')[1].split('.')[0]
                      print('=>', str(kwargs.get('path') + image.name), '<->', str(grayscaleO))
                      if kwargs.get('region') != None:
                          location = pyautogui.locateOnScreen(kwargs.get('path') + image.name, grayscale=grayscaleO, region = kwargs.get('region'))
                      else:
                          location = pyautogui.locateOnScreen(kwargs.get('path') + image.name, grayscale=grayscaleO)
                      if int(cor) not in vexcept:
                          if location != None:
                              break
                  if location != None:
                      response['status'] = True
                      response['location'] = location
                      response['flag'] = int(cor)
                      break
                  else:
                      if kwargs.get('function') != None:
                          eval(str(kwargs.get('function')))
                      else:
                          pyautogui.moveRel(-1,0)
      
              return response
      
          '''
          Retorna coordenadas de una imagen con distintos grados de precision
          '''
          def imagesearch(self, image, precision=0.8):
              im = pyautogui.screenshot()
              img_rgb = np.array(im)
              img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
              template = cv2.imread(image, 0)
              template.shape[::-1]
      
              res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
              min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
              if max_val < precision:
                  return [-1,-1]
              return max_loc
      
          def findImagePre(self, *args, **kwargs):
              response = {}
              response['items'] = []
              timeStart = 0
              timeO = kwargs.get('timeout') if kwargs.get('timeout') != None else self.timeOut
              grayscaleO = kwargs.get('grayscale') if kwargs.get('grayscale') != None else True
              unique = kwargs.get('unique') if kwargs.get('unique') != None else False
              ValoresLocation = namedtuple('ValoresLocation',['x', 'y', 'index', 'status'])
              img_exists = False
              if not unique:
                  for image in os.scandir(kwargs.get('path')):
                      img = cv2.imread(kwargs.get('path') + image.name)
                      height, width, channels = img.shape
      
                      cor = image.name.split('_')[1].split('.')[0]
                      location = self.imagesearch(kwargs.get('path') + image.name)
      
                      img_exists = True if(location[0] >= 0 and location[1] >= 0) else False
      
                      if location != None:
                          response['items'].append({
                              'location': ValoresLocation(
                                      (location[0] + (width / 2)) if img_exists else -1, 
                                      (location[1] + (height / 2)) if img_exists else -1,
                                      int(cor) if img_exists else -1,
                                      img_exists)
                          })
              else:
                  response = {}
                  while timeStart != timeO:
                      time.sleep(0.01)
                      timeStart = timeStart + 1
                      cor = 0
                      location = None
                      for image in os.scandir(kwargs.get('path')):
                          img = cv2.imread(kwargs.get('path') + image.name)
                          height, width, channels = img.shape
      
                          cor = image.name.split('_')[1].split('.')[0]
                          location = self.imagesearch(kwargs.get('path') + image.name)
                          print('=>', str(kwargs.get('path') + image.name), '<->', str(grayscaleO))
                          img_exists = True if(location[0] >= 0 and location[1] >= 0) else False
                          if img_exists:
                              break
      
                      if img_exists:
                          response['status'] = True
                          response['location'] = ValoresLocation(
                                      (location[0] + (width / 2)) if img_exists else -1, 
                                      (location[1] + (height / 2)) if img_exists else -1,
                                      int(cor) if img_exists else -1,
                                      img_exists
                          )
                          break
                      else:
                          response['status'] = False
                          pyautogui.moveRel(-1,0)
      
              return response
      
          def startApp(self, *args, **kwargs):
              os.system('start chrome "http://10.19.17.40/PRAXIS/"')
      
              try:
                  pyautogui.FAILSAFE = True
                  time.sleep(2)
                  confirma = pyautogui.confirm(text='Iniciar proceso automatizado?', title='Robot', buttons=['OK', 'Cancel'])
                  if confirma == 'OK':
                      rs = self.getData(
                          IN_USER = 'GUESTPRX'
                      )
                      for row in rs:
                          print(row)
                          self.paso_0001(row)
      
                      pyautogui.alert(text='El proceso culminó exitósamente', title='Robot BSP', button='OK')
                  else:
                      print('Proceso cancelado!')
              except pyautogui.FailSafeException as err:
                  print(err)
                  pyautogui.alert(text='Se interrumpió el proceso!', title='Robot', button='OK')
      
          def getData(self, *args, **kwargs):
              query = """\
                  select USR, CITY, USCR from PRAXIS.INF001
                  where USR = ?
              """
              params = (
                  kwargs.get('IN_USER'),
              )
              rs = self.conn.execute(query,params).fetchall()
              return rs
      
          def paso_0001(self, row):
              print('paso_0001')
              self.objExiste = self.findImage(path=self.path_robot + '/images/img_001/')
              if self.objExiste['status']:
                  self.paso_0002(row)
      
          def paso_0002(self, row):
              print('paso_0002')
              self.objExiste = self.findImage(path=self.path_robot + '/images/img_002/')
              if self.objExiste['status']:
                  locationx, locationy = pyautogui.center(self.objExiste['location'])
                  pyautogui.moveTo(locationx + 50, locationy)
                  time.sleep(1)
                  pyautogui.click(clicks=2, interval=0.5)
                  
                  time.sleep(1)
                  keyboard.send('ctrl+a,del')
                  time.sleep(1)
                  pyautogui.typewrite('GUESTPRX')
                  pyautogui.press('tab')
                  time.sleep(1)
      
                  keyboard.send('ctrl+a,del')
                  time.sleep(1)
                  pyautogui.typewrite('AS2015')
                  pyautogui.press('tab')
                  time.sleep(1)
      
                  keyboard.send('enter')
      
                  self.paso_0003(row)
      
          def paso_0003(self, row):
              print('paso_0003')
              self.objExiste = self.findImage(path=self.path_robot + '/images/img_003/')
              if self.objExiste['status']:
                  locationx, locationy = pyautogui.center(self.objExiste['location'])
                  pyautogui.moveTo(locationx, locationy)
                  time.sleep(1)
                  pyautogui.click()
      
                  self.paso_0004(row)
      
          def paso_0004(self, row):
              print('paso_0004')
              self.objExiste = self.findImage(path=self.path_robot + '/images/img_004/')
              if self.objExiste['status']:
                  self.log_screen()
      
          def log_screen(self):
              im = pyautogui.screenshot()
              im.save(self.path_robot + '\\screenshot\\' + time.strftime("%Y%m%d") + '_img.png')
              list_of_pixels = list(im.getdata())
              im2 = Image.new(im.mode, im.size)
              im2.putdata(list_of_pixels)
      
      if __name__ == '__main__':
          app = RobotLogin()
          app.startApp()
      ```

      

15. ### Interfaces gráficas

    - **[wxPython](https://wxpython.org/)**

      wxPython es un contenedor para la API de GUI multiplataforma wxWidgets para el lenguaje de programación Python. Si has instalado el archivo **requirements.txt** que indiqué anteriormente, ya deberías tener instalado este módulo. 

    - **[wxFormBuilder](https://ci.appveyor.com/api/projects/jhasse/wxformbuilder-461d5/artifacts/wxFormBuilder_win32.zip?branch=master)**

      wxFormBuilder es una aplicación de diseño de GUI de código abierto para el kit de herramientas wxWidgets, que permite crear aplicaciones ,multiplataforma.

      #### Ejemplo:

      A continuación vamos a agregar una interfaz gráfica al programa realizado anteriormente, para lo cual tenemos que diseñar en wxFormBuilder y que este programa nos genere el código necesario para la GUI

       ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0008.png)

      ![Alt text](D:\remicioluis\repositories\curso-basico-python-robots\images\img_0009.png)

      - **Source code:**

        Archivo "main.py"

        ```python
        # -*- encoding: utf-8 -*-
        
        import os, sys
        import wx
        import wx.aui
        
        sys.path.append(os.path.dirname(os.path.abspath(__file__)))
        
        from form import DemoGUI
        
        class ProcessAdminRobotApp(wx.App):
        
            def OnInit(self):
                frame = DemoGUI(None)
                frame.Show(True)
                return True
        
        if __name__ == '__main__':
            app = ProcessAdminRobotApp(0)
            app.MainLoop()
        ```

        Archivo "form.py"

        ```python
        # -*- coding: utf-8 -*- 
        
        from robot import RobotLogin
        
        ###########################################################################
        ## Python code generated with wxFormBuilder (version Aug  8 2018)
        ## http://www.wxformbuilder.org/
        ##
        ## PLEASE DO *NOT* EDIT THIS FILE!
        ###########################################################################
        
        import wx
        import wx.xrc
        
        ###########################################################################
        ## Class DemoGUI
        ###########################################################################
        
        class DemoGUI ( wx.Frame ):
            
            def __init__( self, parent ):
                wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Demo Ingreso PRAXIS", pos = wx.DefaultPosition, size = wx.Size( 408,239 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
                
                self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
                
                bSizer1 = wx.BoxSizer( wx.VERTICAL )
                
                self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
                bSizer3 = wx.BoxSizer( wx.HORIZONTAL )
                
                self.btn_ingresar = wx.Button( self.m_panel1, wx.ID_ANY, u"Ingresar", wx.DefaultPosition, wx.DefaultSize, 0 )
                bSizer3.Add( self.btn_ingresar, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
                
                
                self.m_panel1.SetSizer( bSizer3 )
                self.m_panel1.Layout()
                bSizer3.Fit( self.m_panel1 )
                bSizer1.Add( self.m_panel1, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
                
                
                self.SetSizer( bSizer1 )
                self.Layout()
                
                self.Centre( wx.BOTH )
                
                # Connect Events
                self.btn_ingresar.Bind( wx.EVT_BUTTON, self.OnIngresarClick )
            
            def __del__( self ):
                pass
            
            
            # Virtual event handlers, overide them in your derived class
            def OnIngresarClick( self, event ):
                event.Skip()
        
                app = RobotLogin()
                app.startApp()
        ```

16. ### Generación de ejecutable

    - **[PyInstaller](https://www.pyinstaller.org/)**

      PyInstaller agrupa una aplicación Python y todas sus dependencias en un solo paquete. El usuario puede ejecutar la aplicación empaquetada sin instalar un intérprete de Python o cualquier módulo.

      Para empaquetar un proyecto, tenemos que abrir la terminal, ubicarnos en el directorio raíz del proyecto, y ejecutar el siguiente comando sobre el archivo principal: 

      ``pyinstaller main.py --onefile``

      El cual generará un .exe en el directorio "/dist"

      

17. ### Source code

    [https://gitlab.com/luis-remicio/curso-basico-python-robots](https://gitlab.com/luis-remicio/curso-basico-python-robots)

18. ### Links de interés

    - [https://www.python.org/](https://www.python.org/)

    - [https://docs.python.org/3.4/index.html](https://docs.python.org/3.4/index.html)

    - [https://pyautogui.readthedocs.io/en/latest/](https://pyautogui.readthedocs.io/en/latest/)

    - [https://wxpython.org/](https://wxpython.org/)

    - [https://www.pyinstaller.org/](https://www.pyinstaller.org/)

      

      